<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Contato extends Model
{
    protected $table = 'contato';

    protected $guarded = ['id'];

    public static function assuntos()
    {
        return [
            'Solicitação de Orçamento',
            'Reclamações',
            'Sugestões',
            'Informações',
        ];
    }
}
