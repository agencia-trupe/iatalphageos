<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CertificadoPDF extends Model
{
    protected $table = 'certificados_pdf';

    protected $guarded = ['id'];

    public function getCreatedAtOrderAttribute()
    {
        return \Carbon\Carbon::createFromFormat('d/m/Y', $this->created_at)->format('Y-m-d');
    }
}
