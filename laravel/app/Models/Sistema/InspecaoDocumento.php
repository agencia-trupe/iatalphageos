<?php

namespace App\Models\Sistema;

use Illuminate\Database\Eloquent\Model;
use App\Models\Auditoria\Usuario as UsuarioAuditoria;

class InspecaoDocumento extends Model
{
    protected $table = 'sistema_inspecoes_documentos';

    protected $guarded = ['id'];

    public function scopeOrdenados($query)
    {
        return $query->orderBy('ordem', 'ASC')->orderBy('id', 'DESC');
    }

    public function inspecao()
    {
        return $this->belongsTo(Inspecao::class, 'inspecao_id');
    }

    public function auditor()
    {
        return $this->belongsTo(UsuarioAuditoria::class, 'auditor_id');
    }
}
