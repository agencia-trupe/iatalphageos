<?php

namespace App\Models\Sistema;

use Illuminate\Database\Eloquent\Model;

class QualidadeDocumento extends Model
{
    protected $table = 'sistema_qualidade_documentos';

    protected $guarded = ['id'];

    public function scopeOrdenados($query)
    {
        return $query->orderBy('ordem', 'ASC')->orderBy('id', 'DESC');
    }

    public function categoria()
    {
        return $this->belongsTo(QualidadeCategoria::class, 'categoria_id');
    }

    public function permissoes()
    {
        return $this->morphedByMany(
            \App\Models\Auditoria\Usuario::class,
            'usuario',
            'sistema_qualidade_permissoes'
        );
    }
}
