<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\CropImage;

use Cviebrock\EloquentSluggable\SluggableInterface;
use Cviebrock\EloquentSluggable\SluggableTrait;

class Pagina extends Model implements SluggableInterface
{
    use SluggableTrait;

    protected $sluggable = [
        'build_from' => 'titulo',
        'save_to'    => 'slug',
        'on_update'  => true
    ];

    protected $table = 'paginas';

    protected $guarded = ['id', 'pagina_interna'];

    public static function upload_imagem()
    {
        return CropImage::make('imagem', [
            'width'  => 580,
            'height' => null,
            'path'   => 'assets/img/paginas/'
        ]);
    }

    public function selos()
    {
        return $this->hasMany(Selo::class, 'pagina_id')->ordenados();
    }
}
