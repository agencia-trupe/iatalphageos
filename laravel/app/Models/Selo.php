<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\CropImage;

class Selo extends Model
{
    protected $table = 'selos';

    protected $guarded = ['id'];

    public function scopeOrdenados($query)
    {
        return $query->orderBy('ordem', 'ASC')->orderBy('id', 'DESC');
    }

    public static function upload_imagem()
    {
        return CropImage::make('imagem', [
            'width'  => null,
            'height' => 140,
            'path'   => 'assets/img/selos/'
        ]);
    }

    public function scopePagina($query, $id)
    {
        return $query->where('pagina_id', $id);
    }
}
