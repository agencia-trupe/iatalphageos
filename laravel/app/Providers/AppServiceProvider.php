<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        view()->composer([
            'frontend.common.template',
            'admin.common.template',
            'auditoria.common.template',
            'inspecao.common.template',
        ], function($view) {
            $paginas = \App\Models\Pagina::where('pagina_interna', 0)->get();

            $contato = new \stdClass();
            $contato->slug = 'contato';
            $contato->titulo = 'Contato';

            $paginas->push($contato);

            $view->with('paginas', $paginas);
            $view->with('contato', \App\Models\Contato::first());
        });

        view()->composer('*', function($view) {
            $view->with('config', \App\Models\Configuracoes::first());
        });

        view()->composer('painel.common.nav', function($view) {
            $view->with('contatosNaoLidos', \App\Models\ContatoRecebido::naoLidos()->count());
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
