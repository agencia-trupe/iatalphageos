<?php

namespace App\Providers;

use Illuminate\Routing\Router;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;

class RouteServiceProvider extends ServiceProvider
{
    /**
     * This namespace is applied to the controller routes in your routes file.
     *
     * In addition, it is set as the URL generator's root namespace.
     *
     * @var string
     */
    protected $namespace = 'App\Http\Controllers';

    /**
     * Define your route model bindings, pattern filters, etc.
     *
     * @param  \Illuminate\Routing\Router  $router
     * @return void
     */
    public function boot(Router $router)
    {
		$router->model('certificados_pdf', 'App\Models\CertificadoPDF');
		$router->model('selos', 'App\Models\Selo');
		$router->model('politicas', 'App\Models\Politica');
		$router->model('paginas', 'App\Models\Pagina');
		$router->model('configuracoes', 'App\Models\Configuracoes');
        $router->model('recebidos', 'App\Models\ContatoRecebido');
        $router->model('contato', 'App\Models\Contato');
        $router->model('usuarios', 'App\Models\User');

        parent::boot($router);
    }

    /**
     * Define the routes for the application.
     *
     * @param  \Illuminate\Routing\Router  $router
     * @return void
     */
    public function map(Router $router)
    {
        $router->group([
            'namespace'  => $this->namespace,
            'middleware' => ['web']
        ], function ($router) {
            require app_path('Http/Routes/admin.php');
            require app_path('Http/Routes/auditoria.php');
            require app_path('Http/Routes/inspecao.php');
            require app_path('Http/Routes/painel.php');
            require app_path('Http/Routes/public.php');
        });
    }
}
