<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class PoliticasRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $rules = [
            'titulo' => 'required',
            'arquivo' => 'required',
        ];

        if ($this->method() != 'POST') {
        }

        return $rules;
    }
}
