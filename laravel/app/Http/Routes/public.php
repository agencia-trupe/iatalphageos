<?php

Route::get('/', 'HomeController@index')->name('home');
Route::get('certificados/pdf/{certificado}', 'HomeController@openCertificadoPdf')->name('certificados.pdf');
Route::get('contato', 'ContatoController@index')->name('contato');
Route::post('contato', 'ContatoController@post')->name('contato.post');
Route::get('login', function() {
    return view('frontend.login');
})->name('login');
Route::get('{pagina}', 'HomeController@pagina')->name('pagina');
