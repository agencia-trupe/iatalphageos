<?php

namespace App\Http\Controllers\Painel;

use App\Helpers\Tools;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\CertificadoPDF;

class CertificadosPDFController extends Controller
{
    private $pathDocumento = 'assets/certificados/';

    public function index()
    {
        $certificados = CertificadoPDF::orderBy('created_at', 'desc')->get();

        return view('painel.certificados-pdf.index', compact('certificados'));
    }

    public function create()
    {
        return view('painel.certificados-pdf.create');
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'titulo'  => 'required',
            'arquivo' => 'required|file'
        ]);

        try {
            CertificadoPDF::create([
                'slug'  => str_slug($request->titulo, '-'),
                'titulo'  => $request->titulo,
                'arquivo' => Tools::fileUpload('arquivo', $this->pathDocumento)
            ]);

            return redirect()->route('painel.certificados-pdf.index')->with('success', 'Arquivo adicionado com sucesso.');
        } catch (\Exception $e) {
            return back()->withErrors(['Erro ao adicionar arquivo: ' . $e->getMessage()]);
        }
    }

    public function edit(CertificadoPDF $certificado)
    {
        return view('painel.certificados-pdf.edit', compact('certificado'));
    }

    public function update(Request $request, CertificadoPDF $certificado)
    {
        $this->validate($request, [
            'titulo'  => 'required',
            'arquivo' => 'file'
        ]);

        try {
            $input = $request->all();
            $input['slug'] = str_slug($request->titulo, '-');

            if (isset($input['arquivo'])) {
                $input['arquivo'] = Tools::fileUpload('arquivo', $this->pathDocumento);
            }

            $certificado->update($input);

            return redirect()->route('painel.certificados-pdf.index')->with('success', 'Arquivo alterado com sucesso.');
        } catch (\Exception $e) {
            return back()->withErrors(['Erro ao alterar arquivo: ' . $e->getMessage()]);
        }
    }

    public function destroy(CertificadoPDF $certificado)
    {
        try {
            $certificado->delete();

            return redirect()->route('painel.certificados-pdf.index')->with('success', 'Arquivo excluído com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir arquivo: ' . $e->getMessage()]);
        }
    }
}
