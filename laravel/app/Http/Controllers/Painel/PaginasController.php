<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\PaginasRequest;
use App\Http\Controllers\Controller;

use App\Models\Pagina;

class PaginasController extends Controller
{
    public function index()
    {
        $registros = Pagina::where('pagina_interna', 0)->get();
        $internas  = Pagina::where('pagina_interna', 1)->get();

        return view('painel.paginas.index', compact('registros', 'internas'));
    }

    public function create()
    {
        return view('painel.paginas.create');
    }

    public function store(PaginasRequest $request)
    {
        try {

            $input = [
                'titulo' => $request->get('titulo'),
                'texto'  => $request->get('texto')
            ];

            if ($request->hasFile('imagem')) $input['imagem'] = Pagina::upload_imagem();

            Pagina::create($input);

            return redirect()->route('painel.paginas.index')->with('success', 'Registro adicionado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar registro: '.$e->getMessage()]);

        }
    }

    public function edit(Pagina $registro)
    {
        return view('painel.paginas.edit', compact('registro'));
    }

    public function update(PaginasRequest $request, Pagina $registro)
    {
        try {

            $input = ['texto' => $request->get('texto')];

            if ($registro->pagina_interna) {
                $input['titulo'] = $request->get('titulo');
            }

            if ($request->hasFile('imagem')) $input['imagem'] = Pagina::upload_imagem();

            $registro->update($input);

            return redirect()->route('painel.paginas.index')->with('success', 'Registro alterado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);

        }
    }

    public function destroy(Pagina $registro)
    {
        try {

            if ($registro->pagina_interna == 0) {
                throw new \Exception('Não é possível excluir essa página.');
            }

            $registro->delete();

            return redirect()->route('painel.paginas.index')->with('success', 'Registro excluído com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: '.$e->getMessage()]);

        }
    }
}
