<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\SelosRequest;
use App\Http\Controllers\Controller;

use App\Models\Selo;
use App\Models\Pagina;

class SelosController extends Controller
{
    public function index(Pagina $pagina)
    {
        $registros = Selo::pagina($pagina->id)->ordenados()->get();

        return view('painel.paginas.selos.index', compact('pagina', 'registros'));
    }

    public function create(Pagina $pagina)
    {
        return view('painel.paginas.selos.create', compact('pagina'));
    }

    public function store(Pagina $pagina, SelosRequest $request)
    {
        try {

            $input = $request->all();

            if (isset($input['imagem'])) $input['imagem'] = Selo::upload_imagem();

            $pagina->selos()->create($input);

            return redirect()->route('painel.paginas.selos.index', $pagina->id)->with('success', 'Registro adicionado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar registro: '.$e->getMessage()]);

        }
    }

    public function edit(Pagina $pagina, Selo $registro)
    {
        return view('painel.paginas.selos.edit', compact('pagina', 'registro'));
    }

    public function update(Pagina $pagina, Selo $registro, SelosRequest $request)
    {
        try {

            $input = $request->all();

            if (isset($input['imagem'])) $input['imagem'] = Selo::upload_imagem();

            $registro->update($input);

            return redirect()->route('painel.paginas.selos.index', $pagina->id)->with('success', 'Registro alterado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);

        }
    }

    public function destroy(Pagina $pagina, Selo $registro)
    {
        try {

            $registro->delete();

            return redirect()->route('painel.paginas.selos.index', $pagina->id)->with('success', 'Registro excluído com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: '.$e->getMessage()]);

        }
    }

}
