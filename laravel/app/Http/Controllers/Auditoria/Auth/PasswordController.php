<?php

namespace App\Http\Controllers\Auditoria\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ResetsPasswords;

class PasswordController extends Controller
{
    use ResetsPasswords;

    protected $broker = 'auditoria';
    protected $redirectPath = 'auditoria/login';
    protected $linkRequestView = 'auditoria.auth.password';
    protected $resetView = 'auditoria.auth.reset';

    public function __construct()
    {
        $this->middleware('auditoria.guest');
    }
}
