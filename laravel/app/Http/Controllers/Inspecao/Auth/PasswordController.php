<?php

namespace App\Http\Controllers\Inspecao\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ResetsPasswords;

class PasswordController extends Controller
{
    use ResetsPasswords;

    protected $broker = 'inspecao';
    protected $redirectPath = 'inspecao/login';
    protected $linkRequestView = 'inspecao.auth.password';
    protected $resetView = 'inspecao.auth.reset';

    public function __construct()
    {
        $this->middleware('inspecao.guest');
    }
}
