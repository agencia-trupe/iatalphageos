<?php

namespace App\Http\Controllers\Inspecao;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ReclamacaoApelacaoController extends Controller
{
    public function index()
    {
        $documentos = auth('inspecao')->user()->documentosReclamacaoApelacao;

        return view('inspecao.reclamacao-apelacao', compact('documentos'));
    }
}
