<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;

use App\Models\Admin\Usuario as UsuarioAdmin;
use App\Models\Auditoria\Usuario as UsuarioAuditoria;
use App\Models\Inspecao\Usuario as UsuarioInspecao;

class UsuariosController extends Controller
{
    public function index()
    {
        $usuarios = collect()
            ->merge(UsuarioAdmin::get())
            ->merge(UsuarioAuditoria::get())
            ->merge(UsuarioInspecao::get());

        return view('admin.usuarios.index', compact('usuarios'));
    }

    public function store(Request $request)
    {
        $rules = [
            'nome'   => 'required',
            'email'  => 'required|email',
            'perfil' => 'required|in:administrador,auditor-de-campo,acesso-inspecao'
        ];

        switch($request->perfil) {
            case 'administrador':
                $rules['email'] = $rules['email'].'|unique:admin_usuarios';
                $class = new UsuarioAdmin;
                break;
            case 'auditor-de-campo':
                $rules['email'] = $rules['email'].'|unique:auditoria_usuarios';
                $class = new UsuarioAuditoria;
                break;
            case 'acesso-inspecao':
                $rules['email'] = $rules['email'].'|unique:inspecao_usuarios';
                $class = new UsuarioInspecao;
                break;
            default:
                abort('404');
        }

        $this->validate($request, $rules);

        try {
            $class->create([
                'nome'     => $request->nome,
                'email'    => $request->email,
                'password' => bcrypt('iatmudar#123')
            ]);
            return redirect()->route('admin.usuarios.index')->with('success', 'Usuário criado com sucesso.');
        } catch (\Exception $e) {
            return back()->withErrors(['Erro ao criar usuário: '.$e->getMessage()]);
        }
    }

    public function destroy($tipo, $id)
    {
        switch($tipo) {
            case 'administrador':
                $usuario = UsuarioAdmin::findOrFail($id);
                break;
            case 'auditor-de-campo':
                $usuario = UsuarioAuditoria::findOrFail($id);
                break;
            case 'acesso-inspecao':
                $usuario = UsuarioInspecao::findOrFail($id);
                break;
            default:
                abort('404');
        }

        try {
            if ($tipo == 'administrador' && $usuario->id == auth('admin')->user()->id) {
                throw new \Exception('Não é possível excluir este usuário.');
            }
            if ($tipo == 'administrador' && $usuario->id == 1) {
                throw new \Exception('Não é possível excluir este usuário.');
            }
            if ($tipo == 'auditor-de-campo' && $usuario->documentos->count()) {
                throw new \Exception('Não é possível excluir um auditor de campo que possui documentos cadastrados.');
            }
            $usuario->delete();
            return redirect()->route('admin.usuarios.index')->with('success', 'Usuário excluído com sucesso.');
        } catch (\Exception $e) {
            return back()->withErrors(['Erro ao excluir usuário: '.$e->getMessage()]);
        }
    }
}
