<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Models\CertificadoPDF;
use App\Models\Pagina;
use App\Models\Politica;

class HomeController extends Controller
{
    public function index()
    {
        return view('frontend.home');
    }

    public function pagina($slug)
    {
        $pagina    = Pagina::whereSlug($slug)->firstOrFail();
        $politicas = $pagina->slug == 'historico'
            ? Politica::ordenados()->get()
            : [];

        return view('frontend.pagina', compact('pagina', 'politicas'));
    }

    public function openCertificadoPdf($slug)
    {
        $arquivo = CertificadoPDF::where('slug', $slug)->select('arquivo')->first();

        $arquivoPath = public_path() . "/assets/certificados/" . $arquivo->arquivo;

        return response()->file($arquivoPath);
    }
}
