<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSelosTable extends Migration
{
    public function up()
    {
        Schema::create('selos', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('pagina_id')->unsigned()->nullable();
            $table->integer('ordem')->default(0);
            $table->string('imagem');
            $table->timestamps();
            $table->foreign('pagina_id')->references('id')->on('paginas')->onDelete('cascade');
        });
    }

    public function down()
    {
        Schema::drop('selos');
    }
}
