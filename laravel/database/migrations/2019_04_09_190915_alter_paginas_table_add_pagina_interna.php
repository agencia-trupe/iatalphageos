<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterPaginasTableAddPaginaInterna extends Migration
{
    public function up()
    {
        Schema::table('paginas', function (Blueprint $table) {
            $table->boolean('pagina_interna')->default(1)->after('imagem');
        });
    }

    public function down()
    {
        Schema::table('paginas', function (Blueprint $table) {
            $table->dropColumn('pagina_interna');
        });
    }
}
