<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDocumentosGeraisPermissoesTable extends Migration
{
    public function up()
    {
        Schema::create('sistema_documentos_gerais_permissoes', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('documento_geral_id')->unsigned();
            $table->foreign('documento_geral_id')->references('id')->on('sistema_documentos_gerais')->onDelete('cascade');

            $table->integer('usuario_id');
            $table->string('usuario_type');

            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('sistema_documentos_gerais_permissoes');
    }
}
