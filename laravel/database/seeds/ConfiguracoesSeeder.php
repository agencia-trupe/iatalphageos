<?php

use Illuminate\Database\Seeder;

class ConfiguracoesSeeder extends Seeder
{
    public function run()
    {
        DB::table('configuracoes')->insert([
            'title' => 'IAT &middot; Instituto Alphageos de Tecnologia',
        ]);
    }
}
