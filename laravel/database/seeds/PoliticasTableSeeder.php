<?php

use Illuminate\Database\Seeder;

class PoliticasTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('politicas')->insert([
            [
                'ordem'   => 0,
                'titulo'  => 'Política de Imparcialidade',
                'arquivo' => 'DQ-420 - Rev.00 - Política de Imparcialidade.pdf',
            ],
            [
                'ordem'   => 1,
                'titulo'  => 'Política de Confidencialidade',
                'arquivo' => 'DQ-421 - Rev.00 - Política de Confidencialidade.pdf',
            ],
            [
                'ordem'   => 2,
                'titulo'  => 'Política da Qualidade',
                'arquivo' => 'FOR-811 - Rev.00 - Política e Objetivo da Qualidade.pdf',
            ],
            [
                'ordem'   => 3,
                'titulo'  => 'Código de Ética',
                'arquivo' => 'DQ-422 - Rev.00 - Código de Ética IAT.pdf',
            ],
        ]);
    }
}
