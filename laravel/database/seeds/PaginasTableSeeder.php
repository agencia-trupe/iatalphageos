<?php

use Illuminate\Database\Seeder;

class PaginasTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('paginas')->insert([
            [
                'titulo' => 'Histórico',
                'slug'   => 'historico',
                'texto'  => '<p>O <strong>IAT - INSTITUTO ALPHAGEOS DE TECNOLOGIA</strong>, fundado em 28 de Setembro de 2018 com o objetivo de fomentar a qualidade e a inovação tecnológica nas mais diversas áreas da indústria Brasileira, faz parte do <strong>Grupo Alphageos</strong> que a mais de 30 anos atua nas áreas de sondagens a percussão, projetos de fundações, escavações, terraplenos e outras obras civis, como sondagem rotativa, e de controle tecnológico (concreto, solos e pavimentos), acompanhamento da construção, inclusive de obras subterrâneas com mapeamento das frentes de escavação, instrumentação geotécnica, supervisão, fiscalização e auditoria da qualidade de obras. </p><p>Com o mesmo ideal empreendedor e focado no bem-estar social, segurança e meio ambiente, através de serviços de excelência a seus clientes, o <strong>IAT - INSTITUTO ALPHAGEOS DE TECNOLOGIA</strong> privilegia a consistência técnica de suas certificações, inspeções, ensaios e avaliações.</p>',
                'imagem' => 'img-empresa.jpg',
                'pagina_interna' => 0
            ],
            [
                'titulo' => 'Certificação de Produtos',
                'slug'   => 'certificacao-de-produtos',
                'texto'  => '<p>A Certificação de produtos tem como objetivo atestar que um produto atende aos critérios de uma Norma Técnica.</p><p>Esta Certificação deve ser realizada por um Organismo independente, de terceira parte, devidamente Acreditado para realizar este processo que inclui: análise técnica da documentação, auditoria na fábrica, amostragem do produto, realização dos ensaios, entre outras etapas que podem existir de acordo com o modelo de certificação requerido.</p><p>Uma vez todas as etapas do processo estando conformes, o Organismo emite um Certificado de Conformidade e passa a realizar manutenções periódicas para averiguar se o produto continua atendendo aos critérios estabelecidos na Norma Técnica.</p><p>A Certificação de produtos visa atestar que não somente uma amostra, mas toda a linha de produção de um determinado produto está conforme as Normas Técnicas específicas e é relativa a verificação da segurança, performance e / ou eficiência energética do produto.</p>',
                'imagem' => 'img-certProdutos.png',
                'pagina_interna' => 0
            ],
            [
                'titulo' => 'Certificação de Sistemas de Gestão',
                'slug'   => 'certificacao-de-sistemas-de-gestao',
                'texto'  => '<p>A Certificação de Sistemas de Gestão tem como objetivo atestar que uma empresa possui um Sistema de Gestão que atende aos critérios de uma Norma Técnica de Gestão que pode ser, entre outras:</p><ul><li>Sistema de Gestão da Qualidade – ISO 9001;</li><li>Sistema de Gestão Ambiental – ISO 14001;</li><li>Sistema de Gestão de Segurança e Saúde no Trabalho – OHSAS 18001</li><li>Sistema de Gestão da Segurança Alimentar – ISO 22000;</li></ul><p>Esta Certificação deve ser realizada por um Organismo independente, de terceira parte, devidamente Acreditado para realizar este processo que inclui: análise técnica da documentação, avaliação do processo produtivo, e uma vez que não haja Não Conformidades que impeçam a certificação, o Organismo emite um Certificado de Conformidade e passa a realizar manutenções periódicas para averiguar se o processo produtivo continua atendendo aos critérios estabelecidos na Norma Técnica.</p>',
                'imagem' => 'img-certSistGestao.png',
                'pagina_interna' => 0
            ],
            [
                'titulo' => 'Inspeções',
                'slug'   => 'inspecoes',
                'texto'  => '<p>A Inspeção Acreditada é usada mundialmente para aumentar a previsibilidade (Engenharia + Econômico-financeira); mitigar riscos (inclusive jurídicos) e atestar a conformidade técnica (Ambiental, Normas e Regulamentos).</p><p>O INMETRO, através da Portaria nº 367, de 20/12/2017, regulamentou  a Inspeção Acreditada de Projetos e Obras de Infraestrutura no Brasil.</p><p>A Secretaria Especial do Programa de Parcerias de Investimentos (SPPI), do governo Brasileiro publicou a Orientação Normativa Nº 1, de 20/12/2017, recomendando a utilização da Inspeção Acreditada como boa prática de mercado.</p><p>Os setores abrangidos pela Inspeção Acreditada de Projetos e Obras de Infraestrutura são:</p><ul><li>Aeroportos</li><li>Ferrovias</li><li>Rodovias</li><li>Portos</li><li>Geração Hidrelétrica</li><li>Transmissão de Energia</li><li>Distribuição de Energia</li><li>Mineração</li><li>Exploração de Óleo e Gás</li><li>Iluminação Pública</li><li>Habitacionais</li><li>Saneamento</li></ul><p>O processo de contratação de Organismo de Inspeção pode ocorrer diretamente pelo Poder Público, pelo licitante ou através do mercado privado diretamente.</p>',
                'imagem' => 'img-inspecao.png',
                'pagina_interna' => 0
            ],
            [
                'titulo' => 'Avaliações Técnicas',
                'slug'   => 'avaliacoes-tecnicas',
                'texto'  => '<ul><li>Sustentabilidade</li><li>Eficiência Energética</li><li>Qualidade da Água</li><li>Qualidade do Ar</li><li>Eficiência no uso da Água</li><li>Programas de Avaliação Setorial – Segurança, Desempenho, Eficiência</li><li>Avaliação de Fornecedores</li><li>Análise de Ciclo de Vida</li><li>Ensaios</li></ul>',
                'imagem' => 'img-avaliacoesTecnicas.png',
                'pagina_interna' => 0
            ],
            [
                'titulo' => 'Treinamento',
                'slug'   => 'treinamento',
                'texto'  => '<ul><li>ISO 9000 - Qualidade</li><li>ISO 9000 + SIAC – PBQPH – Qualidade na Construção Civil</li><li>ISO 14000 - Ambiental</li><li>ISO 18000 – Saúde e Segurança Ocupacional</li><li>ISO 17021 – Acreditação de Sistemas de Gestão</li><li>ISO 17065 -  Acreditação de Produtos</li><li>ISO 17020 – Inspeção</li><li>ISO 17025 – Acreditação de Laboratórios</li></ul>',
                'imagem' => 'img-treinamento.png',
                'pagina_interna' => 0
            ],
        ]);
    }
}
