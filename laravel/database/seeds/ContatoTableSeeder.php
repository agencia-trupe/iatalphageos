<?php

use Illuminate\Database\Seeder;

class ContatoTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('contato')->insert([
            'email' => 'contato@trupe.net',
            'telefone' => '11 4196 5400',
            'endereco' => 'Rua João Ferreira de Camargo, 601 - Tamboré<br>06460-060 - Barueri, SP',
            'google_maps' => '<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3658.881294656587!2d-46.821209484494965!3d-23.500784765244365!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x94cefe06eb15e3f1%3A0xd739ecc1a3ded8cd!2sR.+Jo%C3%A3o+Ferreira+de+Camargo%2C+601+-+Jardim+Mutinga%2C+Barueri+-+SP%2C+06460-060!5e0!3m2!1spt-BR!2sbr!4v1553802621242!5m2!1spt-BR!2sbr" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>'
        ]);
    }
}
