export default function InspecaoPermissoes() {
    if (!$('.form-inspecao-permissoes').length) return;

    const usuariosOptionsClone = $('select[name=usuario] option').clone();

    function reset() {
        $('select[name=perfil] option:first-child').attr('selected', true);
        $('select[name=usuario]')
            .html(usuariosOptionsClone)
            .attr('disabled', true)
            .val('')
            .change();
    }

    function filterUsuarios(perfil) {
        return usuariosOptionsClone.filter(
            (index, option) =>
                $(option).data('perfil') === perfil || $(option).val() === ''
        );
    }

    $('select[name=perfil]').change(function() {
        const perfil = $(this).val();

        if (!perfil) return reset();

        $('select[name=usuario]')
            .html(filterUsuarios(perfil))
            .attr('disabled', false)
            .val('')
            .change();
    });

    reset();
}
