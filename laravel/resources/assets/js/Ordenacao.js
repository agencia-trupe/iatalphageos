export default function Ordenacao() {
    $('.btn-ordem').click(function(e) {
        e.preventDefault();
    });
    sortable('.table-sortable tbody', {
        handle: '.btn-ordem',
        forcePlaceholderSize: true,
        placeholderClass: 'placeholder'
    });
    $(sortable('.table-sortable tbody')).on('sortupdate', function(e) {
        var url = $(this)
                .parent()
                .attr('data-order-url'),
            table = $(this)
                .parent()
                .attr('data-table'),
            data = [];

        $(this)
            .children('tr')
            .each(function(index, el) {
                data.push(el.id);
            });

        $.post(url, { data: data, table: table });
    });
}
