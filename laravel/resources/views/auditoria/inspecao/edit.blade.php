@extends('auditoria.common.template')

@section('content')

    <main class="inspecao">
        <div class="center">
            <h2 class="title">
                INSPEÇÃO -
                {{ $inspecao->cliente }} |
                {{ $inspecao->obra }} |
                {{ $inspecao->os }}
            </h2>

            @include('auditoria.common._flash')

            <form action="{{ route('auditoria.inspecao.update', [$inspecao->id, $inspecaoDocumento->id]) }}" method="POST" enctype="multipart/form-data" class="form-documento-admin">
                {!! csrf_field() !!}
                {!! method_field('PATCH') !!}

                <div class="row">
                    <span><strong>ALTERAR DOCUMENTO:</strong></span>
                    <label class="file-input">
                        <span>SELECIONAR DOCUMENTO</span>
                        <input type="file" name="arquivo">
                    </label>
                </div>
                <div class="row link-documento-editar">
                    <span></span>
                    <a href="{{ asset('assets/sistema/inspecao/'.$inspecaoDocumento->arquivo) }}" target="_blank">{{ $inspecaoDocumento->arquivo }}</a>
                </div>
                <div class="row">
                    <span>descrição/título:</span>
                    <input type="text" name="titulo" value="{{ $inspecaoDocumento->titulo }}" required>
                </div>
                <div class="row">
                    <span>observações:</span>
                    <input type="text" name="observacoes" value="{{ $inspecaoDocumento->observacoes }}">
                </div>
                <div class="row">
                    <span></span>
                    <div class="btn-inline-group">
                        <input type="submit" value="SALVAR" class="btn btn-primary">
                        <a href="{{ route('auditoria.inspecao.show', $inspecao->id) }}" class="btn btn-default">VOLTAR</a>
                    </div>
                </div>
            </form>
        </div>
    </main>

@endsection
