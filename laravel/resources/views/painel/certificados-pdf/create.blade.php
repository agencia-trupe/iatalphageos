@extends('painel.common.template')

@section('content')

<legend>
    <h2><small>Certificados PDF |</small> Adicionar Arquivo</h2>
</legend>

{!! Form::open(['route' => 'painel.certificados-pdf.store', 'files' => true]) !!}

@include('painel.certificados-pdf.form', ['submitText' => 'Inserir'])

{!! Form::close() !!}

@endsection