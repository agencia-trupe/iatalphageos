@extends('painel.common.template')

@section('content')

<legend>
    <h2><small>Certificados PDF |</small> Editar Arquivo</h2>
</legend>

{!! Form::model($certificado, [
'route' => ['painel.certificados-pdf.update', $certificado->id],
'method' => 'patch',
'files' => true])
!!}

@include('painel.certificados-pdf.form', ['submitText' => 'Alterar'])

{!! Form::close() !!}

@endsection