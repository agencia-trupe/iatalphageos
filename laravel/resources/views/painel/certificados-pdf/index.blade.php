@extends('painel.common.template')

@section('content')

@include('painel.common.flash')

<legend>
    <h2>
        Certificados PDF
        <a href="{{ route('painel.certificados-pdf.create') }}" class="btn btn-success btn-sm pull-right"><span class="glyphicon glyphicon-plus" style="margin-right:10px;"></span>Adicionar Arquivo</a>
    </h2>
</legend>


@if(!count($certificados))
<div class="alert alert-warning" role="alert">Nenhum registro encontrado.</div>
@else
<table class="table table-striped table-bordered table-hover table-info table-sortable" data-table="certificados_pdf">
    <thead>
        <tr>
            <th>Data</th>
            <th>Título</th>
            <th>Link PDF</th>
            <th class="no-filter"><span class="glyphicon glyphicon-cog"></span></th>
        </tr>
    </thead>

    <tbody>
        @foreach ($certificados as $certificado)
        <tr class="tr-row" id="{{ $certificado->id }}">
            <td>{{ $certificado->created_at->format('d/m/Y') }}</td>
            <td>{{ $certificado->titulo }}</td>
            <td>
                <a href="{{ route('certificados.pdf', $certificado->slug) }}" target="_blank">{{ route('certificados.pdf', $certificado->slug) }}</a>
            </td>
            <td class="crud-actions">
                {!! Form::open([
                'route' => ['painel.certificados-pdf.destroy', $certificado->id],
                'method' => 'delete'
                ]) !!}

                <div class="btn-group btn-group-sm">
                    <a href="{{ route('painel.certificados-pdf.edit', $certificado->id ) }}" class="btn btn-primary btn-sm pull-left">
                        <span class="glyphicon glyphicon-pencil" style="margin-right:10px;"></span>Editar
                    </a>
                    <button type="submit" class="btn btn-danger btn-sm btn-delete"><span class="glyphicon glyphicon-remove" style="margin-right:10px;"></span>Excluir</button>
                </div>

                {!! Form::close() !!}
            </td>
        </tr>
        @endforeach
    </tbody>
</table>
@endif

@endsection