@include('painel.common.flash')

<div class="form-group">
    {!! Form::label('titulo', 'Titulo') !!}
    {!! Form::text('titulo', null, ['class' => 'form-control']) !!}
</div>

<div class="well form-group">
    {!! Form::label('arquivo', 'Arquivo') !!}
    @if($submitText == 'Alterar')
    @if($certificado->arquivo)
    <a href="{{ asset('assets/certificados/'.$certificado->arquivo) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">{{ $certificado->arquivo }}</a>
    @endif
    @endif
    {!! Form::file('arquivo', ['class' => 'form-control']) !!}
</div>

<hr>

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}

<a href="{{ route('painel.certificados-pdf.index') }}" class="btn btn-default btn-voltar">Voltar</a>