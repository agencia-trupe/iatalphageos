<ul class="nav navbar-nav">
    <li @if(Tools::routeIs('painel.paginas*')) class="active" @endif>
        <a href="{{ route('painel.paginas.index') }}">Páginas</a>
    </li>
    <li @if(Tools::routeIs('painel.politicas*')) class="active" @endif>
        <a href="{{ route('painel.politicas.index') }}">Políticas</a>
    </li>
    <li @if(Tools::routeIs('painel.certificados-pdf*')) class="active" @endif>
        <a href="{{ route('painel.certificados-pdf.index') }}">Certificados PDF</a>
    </li>
    <li class="dropdown @if(Tools::routeIs('painel.contato*')) active @endif">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
            Contato
            @if($contatosNaoLidos >= 1)
            <span class="label label-success" style="margin-left:3px;">{{ $contatosNaoLidos }}</span>
            @endif
            <b class="caret"></b>
        </a>
        <ul class="dropdown-menu">
            <li @if(Tools::routeIs('painel.contato.index')) class="active" @endif>
                <a href="{{ route('painel.contato.index') }}">Informações de Contato</a>
            </li>
            <li @if(Tools::routeIs('painel.contato.recebidos*')) class="active" @endif>
                <a href="{{ route('painel.contato.recebidos.index') }}">
                    Contatos Recebidos
                    @if($contatosNaoLidos >= 1)
                    <span class="label label-success" style="margin-left:3px;">{{ $contatosNaoLidos }}</span>
                    @endif
                </a>
            </li>
        </ul>
    </li>
</ul>