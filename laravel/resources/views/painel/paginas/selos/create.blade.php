@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>{{ $pagina->titulo }} / Selos /</small> Adicionar Selo</h2>
    </legend>

    {!! Form::open(['route' => ['painel.paginas.selos.store', $pagina->id], 'files' => true]) !!}

        @include('painel.paginas.selos.form', ['submitText' => 'Inserir'])

    {!! Form::close() !!}

@endsection
