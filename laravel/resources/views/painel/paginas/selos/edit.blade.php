@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>{{ $pagina->titulo }} / Selos /</small> Editar Selo</h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.paginas.selos.update', $pagina->id, $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.paginas.selos.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
