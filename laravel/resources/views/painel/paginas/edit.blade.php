@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Páginas /</small> {{ $registro->titulo }}</h2>
    </legend>

    @if($registro->pagina_interna)
    <div class="well">
        Endereço:
        <a href="{{ route('pagina', $registro->slug) }}">
            {{ route('pagina', $registro->slug) }}
        </a>
    </div>
    @endif

    {!! Form::model($registro, [
        'route'  => ['painel.paginas.update', $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.paginas.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
