@extends('painel.common.template')

@section('content')

    @include('painel.common.flash')

    <legend>
        <h2>
            Páginas
            <a href="{{ route('painel.paginas.create') }}" class="btn btn-success btn-sm pull-right"><span class="glyphicon glyphicon-plus" style="margin-right:10px;"></span>Adicionar Página</a>
        </h2>
    </legend>


    @if(!count($registros))
    <div class="alert alert-warning" role="alert">Nenhum registro encontrado.</div>
    @else
    <table class="table table-striped table-bordered table-hover table-info">
        <thead>
            <tr>
                <th>Título</th>
                <th>Selos</th>
                <th class="no-filter"><span class="glyphicon glyphicon-cog"></span></th>
            </tr>
        </thead>

        <tbody>
        @foreach ($registros as $registro)
            <tr class="tr-row" id="{{ $registro->id }}">
                <td>{{ $registro->titulo }}</td>
                <td><a href="{{ route('painel.paginas.selos.index', $registro->id) }}" class="btn btn-info btn-sm">
                    <span class="glyphicon glyphicon-picture" style="margin-right:10px;"></span>Gerenciar
                </a></td>
                <td class="crud-actions">
                    <a href="{{ route('painel.paginas.edit', $registro->id ) }}" class="btn btn-primary btn-sm pull-left">
                        <span class="glyphicon glyphicon-pencil" style="margin-right:10px;"></span>Editar
                    </a>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
    @endif

    @if(count($internas))
    <hr style="margin:2em 0;">
    <h4>Páginas Internas:</h4>
    <table class="table table-striped table-bordered table-hover table-info">
        <thead>
            <tr>
                <th>Título</th>
                <th>Endereço</th>
                <th>Selos</th>
                <th class="no-filter"><span class="glyphicon glyphicon-cog"></span></th>
            </tr>
        </thead>

        <tbody>
        @foreach ($internas as $registro)
            <tr class="tr-row" id="{{ $registro->id }}">
                <td>{{ $registro->titulo }}</td>
                <td>
                    <a href="{{ route('pagina', $registro->slug) }}">
                        {{ route('pagina', $registro->slug) }}
                    </a>
                </td>
                <td><a href="{{ route('painel.paginas.selos.index', $registro->id) }}" class="btn btn-info btn-sm">
                    <span class="glyphicon glyphicon-picture" style="margin-right:10px;"></span>Gerenciar
                </a></td>
                <td class="crud-actions">
                    {!! Form::open([
                        'route'  => ['painel.paginas.destroy', $registro->id],
                        'method' => 'delete'
                    ]) !!}

                    <div class="btn-group btn-group-sm">
                        <a href="{{ route('painel.paginas.edit', $registro->id ) }}" class="btn btn-primary btn-sm pull-left">
                            <span class="glyphicon glyphicon-pencil" style="margin-right:10px;"></span>Editar
                        </a>

                        <button type="submit" class="btn btn-danger btn-sm btn-delete"><span class="glyphicon glyphicon-remove" style="margin-right:10px;"></span>Excluir</button>
                    </div>

                    {!! Form::close() !!}

                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
    @endif

@endsection

