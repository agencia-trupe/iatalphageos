@include('painel.common.flash')

<div class="form-group">
    {!! Form::label('titulo', 'Título') !!}
    {!! Form::text('titulo', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('arquivo', 'Arquivo') !!}
    <div class="well">
        @if($submitText == 'Alterar' && $registro->arquivo)
        <p>
            <a href="{{ url('assets/politicas/'.$registro->arquivo) }}" target="_blank" style="display:block;">{{ $registro->arquivo }}</a>
        </p>
        @endif
        {!! Form::file('arquivo', ['class' => 'form-control']) !!}
    </div>
</div>

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}

<a href="{{ route('painel.politicas.index') }}" class="btn btn-default btn-voltar">Voltar</a>
