@extends('inspecao.common.template')

@section('content')

    <main class="reclamacao-apelacao">
        <div class="center">
            <h2 class="title">RECLAMAÇÃO E APELAÇÃO</h2>

            @if(!count($documentos))
            <div class="info info-md">Nenhum documento encontrado.</div>
            @else
            <div class="table-wrapper">
                <table class="table table-md">
                    <tbody>
                        @foreach($documentos as $documento)
                        <tr>
                            <td class="stretch no-padding">
                                <div class="arquivo-wrapper">
                                    <a href="{{ asset('assets/sistema/reclamacao-apelacao/'.$documento->arquivo) }}" class="icone-arquivo" target="_blank">icone download</a>
                                    {{ $documento->titulo }}
                                </div>
                            </td>
                            <td>
                                {{ $documento->created_at->format('d/m/Y') }}
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
            @endif
        </div>
    </main>

@endsection
