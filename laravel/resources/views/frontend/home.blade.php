@extends('frontend.common.template')

@section('content')

    <div class="home">
        <div class="center">
            <img src="{{ asset('assets/img/layout/img-bannerhome.jpg') }}" alt="">
            <div class="banner">
                <span>CERTIFICAÇÕES, INSPEÇÕES, ENSAIOS E AVALIAÇÕES</span>
            </div>
        </div>
    </div>

@endsection
