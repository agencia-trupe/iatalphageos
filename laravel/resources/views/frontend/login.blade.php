@extends('frontend.common.template')

@section('content')

    <div class="login">
        <div class="center">
            <div class="login-links">
                <a href="{{ route('auditoria.login') }}">
                    <h2>AUDITORIA</h2>
                    <p>Acesso exclusivo<br>para auditores</p>
                    <img src="{{ asset('assets/img/layout/seta-login.png') }}" alt="">
                </a>
                <a href="{{ route('inspecao.login') }}">
                    <h2>INSPEÇÃO</h2>
                    <p>INMETRO &middot; Poder Público<br>Clientes &middot; Concessionárias</p>
                    <img src="{{ asset('assets/img/layout/seta-login.png') }}" alt="">
                </a>
                <a href="{{ route('admin.login') }}">
                    <h2>ADMIN</h2>
                    <p>Acesso exclusivo<br>do IAT</p>
                    <img src="{{ asset('assets/img/layout/seta-login.png') }}" alt="">
                </a>
            </div>
        </div>
    </div>

@endsection
