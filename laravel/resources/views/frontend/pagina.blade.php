@extends('frontend.common.template')

@section('content')

    <div class="institucional">
        <div class="center">
            @if($pagina->imagem)
            <img src="{{ asset('assets/img/paginas/'.$pagina->imagem) }}" alt="">
            @endif
            <div class="texto" @if(!$pagina->imagem) style="float:none;margin:0 auto;" @endif>
                {!! $pagina->texto !!}

                @if(count($pagina->selos))
                <div class="selos">
                    @foreach($pagina->selos as $selo)
                    <img src="{{ asset('assets/img/selos/'.$selo->imagem) }}" alt="">
                    @endforeach
                </div>
                @endif
            </div>
        </div>

        @if($pagina->slug == 'historico')
        <div class="politicas">
            <div class="center">
                <h2>CONFIRA NOSSAS POLÍTICAS</h2>
                <div class="pdfs">
                    @foreach($politicas as $politica)
                    <a href="{{ url('assets/politicas/'.$politica->arquivo) }}" target="_blank">
                        <div class="icone"></div>
                        <span>{{ $politica->titulo }}</span>
                    </a>
                    @endforeach
                </div>
            </div>
        </div>
        @endif
    </div>

@endsection
