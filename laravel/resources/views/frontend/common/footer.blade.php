    <footer>
        <div class="center">
            <div class="links">
                @foreach($paginas->chunk(4) as $chunk)
                <div class="col">
                    @foreach($chunk as $pagina)
                    <a href="{{ $pagina->slug }}">
                        {{ $pagina->titulo }}
                    </a>
                    @endforeach
                </div>
                @endforeach
            </div>
            <div class="informacoes">
                <img src="{{ asset('assets/img/layout/marca-iat.png') }}" alt="" class="iat">
                <div class="informacoes-texto">
                    <p class="telefone">{{ $contato->telefone }}</p>
                    <p>
                        Instituto Alphageos de Tecnologia<br>
                        {!! $contato->endereco !!}
                    </p>
                </div>
            </div>
        </div>
        <div class="copyright">
            <p>
                © {{ date('Y') }} {{ config('app.name') }} - Todos os direitos reservados.
                <span>|</span>
                <a href="http://www.trupe.net" target="_blank">Criação de sites</a>:
                <a href="http://www.trupe.net" target="_blank">Trupe Agência Criativa</a>
            </p>
        </div>
    </footer>
