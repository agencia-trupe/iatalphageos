    <header>
        <div class="center">
            <a href="{{ route('home') }}" class="logo">{{ config('app.name') }}</a>
            <div class="texto">
                <h2>IAT - INSTITUTO ALPHAGEOS DE TECNOLOGIA</h2>
                <p>O parceiro indispensável em aferir a qualidade, segurança,
                desempenho e eficiência de seus produtos, processos e serviços</p>
            </div>
            <a href="http://www.alphageos.com.br/" target="_blank" class="alphageos">
                <p>O IAT é uma empresa do Grupo Alphageos.<br>VISITE O SITE</p>
                <img src="{{ asset('assets/img/layout/marca-alphageos.png') }}" alt="">
            </a>
        </div>
        <nav>
            <div class="center">
                @foreach($paginas as $pagina)
                    @if($pagina->slug == 'contato')
                    <a href="{{ $pagina->slug }}" @if(Tools::routeIs('contato')) class="active" @endif>{{ $pagina->titulo }}</a>
                    @else
                    <a href="{{ $pagina->slug }}" @if(Tools::routeIs('pagina') && Route::current()->pagina == $pagina->slug) class="active" @endif>{{ $pagina->titulo }}</a>
                    @endif
                @endforeach
                <a href="{{ route('login') }}" @if(Tools::routeIs('login')) class="active" @endif>Login</a>
            </div>
        </nav>
    </header>
