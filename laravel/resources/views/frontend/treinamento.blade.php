@extends('frontend.common.template')

@section('content')

    <div class="institucional">
        <div class="center">
            <img src="{{ asset('assets/img/layout/img-treinamento.png') }}" alt="">
            <div class="texto">
                <ul>
                    <li>ISO 9000 - Qualidade</li>
                    <li>ISO 9000 + SIAC – PBQPH – Qualidade na Construção Civil</li>
                    <li>ISO 14000 - Ambiental</li>
                    <li>ISO 18000 – Saúde e Segurança Ocupacional</li>
                    <li>ISO 17021 – Acreditação de Sistemas de Gestão</li>
                    <li>ISO 17065 -  Acreditação de Produtos</li>
                    <li>ISO 17020 – Inspeção</li>
                    <li>ISO 17025 – Acreditação de Laboratórios</li>
                </ul>
            </div>
        </div>
    </div>

@endsection
