@extends('frontend.common.template')

@section('content')

    <div class="contato">
        <div class="center">
            <div class="informacoes">
                <p class="telefone">{{ $contato->telefone }}</p>
                <p>
                    <strong>INSTITUTO ALPHAGEOS DE TECNOLOGIA</strong><br>
                    {!! $contato->endereco !!}
                </p>
            </div>

            <form action="{{ route('contato.post') }}" method="POST">
                @if($errors->any())
                <div class="flash flash-error">
                    @foreach($errors->all() as $error)
                    {!! $error !!}<br>
                    @endforeach
                </div>
                @endif
                @if(session('enviado'))
                <div class="flash flash-success">
                    Mensagem enviada com sucesso!
                </div>
                @endif

                {!! csrf_field() !!}

                <input type="text" name="nome" placeholder="nome" value="{{ old('nome') }}" @if($errors->has('nome')) class="error" @endif>
                <input type="email" name="email" placeholder="e-mail" value="{{ old('email') }}" @if($errors->has('email')) class="error" @endif>
                <input type="text" name="telefone" placeholder="telefone" value="{{ old('telefone') }}" @if($errors->has('telefone')) class="error" @endif>
                <select name="assunto" @if($errors->has('assunto')) class="error" @endif>
                    <option value="">assunto (selecione...)</option>
                    @foreach(\App\Models\Contato::assuntos() as $assunto)
                    <option value="{{ $assunto }}" @if(old('assunto') == $assunto) selected @endif>{{ $assunto }}</option>
                    @endforeach
                </select>

                <textarea name="mensagem" placeholder="mensagem" @if($errors->has('mensagem')) class="error" @endif>{{ old('mensagem') }}</textarea>

                <input type="submit" value="ENVIAR">
            </form>

            <div class="mapa">{!! $contato->google_maps !!}</div>
        </div>
    </div>

@endsection
