@extends('admin.common.template')

@section('content')

    <main class="documentos-gerais">
        <div class="center">
            <h2 class="title">DOCUMENTOS GERAIS</h2>

            @include('admin.common._flash')

            <form action="{{ route('admin.documentosGerais.update', $documentoGeral->id) }}" method="POST" enctype="multipart/form-data" class="form-documento-admin">
                {!! csrf_field() !!}
                {!! method_field('PATCH') !!}

                <div class="row">
                    <span><strong>ALTERAR DOCUMENTO:</strong></span>
                    <label class="file-input">
                        <span>SELECIONAR DOCUMENTO</span>
                        <input type="file" name="arquivo">
                    </label>
                </div>
                <div class="row link-documento-editar">
                    <span></span>
                    <a href="{{ asset('assets/sistema/documentos-gerais/'.$documentoGeral->arquivo) }}" target="_blank">{{ $documentoGeral->arquivo }}</a>
                </div>
                <div class="row">
                    <span>descrição/título:</span>
                    <input type="text" name="titulo" value="{{ $documentoGeral->titulo }}" required>
                    <input type="submit" value="SALVAR" class="btn btn-primary">
                </div>

                <a href="{{ route('admin.documentosGerais.index') }}" class="btn btn-default btn-voltar">VOLTAR</a>
            </form>
        </div>
    </main>

@endsection
