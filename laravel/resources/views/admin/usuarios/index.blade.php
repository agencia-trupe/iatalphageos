@extends('admin.common.template')

@section('content')

    <main class="usuarios">
        <div class="center">
            <h2 class="title">USUÁRIOS</h2>

            @include('admin.common._flash')

            <form action="{{ route('admin.usuarios.store') }}" method="POST">
                {!! csrf_field() !!}

                <div class="row">
                    <label>e-mail:</label>
                    <input type="email" name="email" value="{{ old('email') }}" required>

                    <label>nome completo:</label>
                    <input type="text" name="nome" value="{{ old('nome') }}" required>
                    <select name="perfil" required>
                        <option value="">Perfil (Selecione...)</option>
                        <option value="administrador" @if(old('perfil') == 'administrador') selected @endif>Administrador</option>
                        <option value="auditor-de-campo" @if(old('perfil') == 'auditor-de-campo') selected @endif>Auditor de Campo</option>
                        <option value="acesso-inspecao" @if(old('perfil') == 'inspecao') selected @endif>Acesso Inspeção</option>
                    </select>
                    <input type="submit" value="CRIAR" class="btn btn-primary">
                </div>

                <div class="info">
                    Senha padrão (deve ser alterada pelo usuário após fazer login):
                    <span>iatmudar#123</span>
                </div>
            </form>

            <div class="table-wrapper">
                <table class="table">
                    @foreach($usuarios as $usuario)
                    <tr>
                        <td class="stretch">{{ $usuario->nome }}</td>
                        <td class="stretch">{{ $usuario->email }}</td>
                        <td style="white-space:nowrap">
                            {{ mb_strtoupper($usuario->tipo) }}
                        </td>
                        <td>{{ $usuario->created_at->format('d/m/Y') }}</td>
                        <td>
                            @if($usuario->tipo != 'Administrador' || auth('admin')->user()->id != $usuario->id && $usuario->id != 1)
                            <a href="{{ route('admin.usuarios.destroy', [str_slug($usuario->tipo), $usuario->id]) }}" class="icone-excluir link-excluir">excluir</a>
                            @else
                            <div class="icone-excluir disabled"></div>
                            @endif
                        </td>
                    </tr>
                    @endforeach
                </table>
            </div>
        </div>
    </main>

@endsection
