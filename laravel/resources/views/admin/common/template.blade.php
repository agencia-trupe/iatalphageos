<!doctype html>
<html lang="pt-BR">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="robots" content="index, follow">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <base href="{{ url('/') }}">

    <meta name="author" content="Trupe Agência Criativa">
    <meta name="copyright" content="{{ date('Y') }} Trupe Agência Criativa">
    <meta name="description" content="{{ $config->description }}">
    <meta name="keywords" content="{{ $config->keywords }}">

    <meta property="og:title" content="{{ $config->title }}">
    <meta property="og:description" content="{{ $config->description }}">
    <meta property="og:site_name" content="{{ $config->title }}">
    <meta property="og:type" content="website">
    <meta property="og:url" content="{{ Request::url() }}">
@if($config->imagem_de_compartilhamento)
    <meta property="og:image" content="{{ asset('assets/img/'.$config->imagem_de_compartilhamento) }}">
@endif

    <title>Sistema de Auditoria de Campo &middot; {{ config('app.name') }}</title>

    <link rel="stylesheet" href="{{ asset('assets/css/vendor.main.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/sistema.css') }}">
</head>
<body>
    <header>
        <div class="center">
            <div class="titulo">
                <h2>
                    SISTEMA DE AUDITORIA DE CAMPO
                    <span>ADMINISTRADOR IAT</span>
                </h2>

                @if(auth('admin')->check())
                <p>
                    Olá {{ strtok(auth('admin')->user()->nome, ' ') }}
                    <a href="{{ route('admin.logout') }}">[LOGOUT]</a>
                </p>
                @endif
            </div>

            <a href="{{ route('home') }}" class="logo">
                {{ config('app.name') }}
            </a>

            <div class="texto">
                <h2>IAT - INSTITUTO ALPHAGEOS DE TECNOLOGIA</h2>
                <p>O parceiro indispensável em aferir a qualidade, segurança,
                desempenho e eficiência de seus produtos, processos e serviços</p>
            </div>
        </div>
    </header>

    @if(auth('admin')->check())
    <nav>
        <div class="center">
            <a href="{{ route('admin.documentosGerais.index') }}" @if(Tools::routeIs('admin.documentosGerais*')) class="active" @endif>
                DOCUMENTOS GERAIS
            </a>
            <a href="{{ route('admin.qualidade.index') }}" @if(Tools::routeIs('admin.qualidade*')) class="active" @endif>
                QUALIDADE
            </a>
            <a href="{{ route('admin.inspecao.index') }}" @if(Tools::routeIs('admin.inspecao*')) class="active" @endif>
                INSPEÇÃO
            </a>
            <a href="{{ route('admin.reclamacaoApelacao.index') }}" @if(Tools::routeIs('admin.reclamacaoApelacao*')) class="active" @endif>
                RECLAMAÇÃO E APELAÇÃO
            </a>
            <a href="{{ route('admin.usuarios.index') }}" @if(Tools::routeIs('admin.usuarios*')) class="active" @endif>
                USUÁRIOS
            </a>
            <a href="{{ route('admin.alterarSenha') }}" @if(Tools::routeIs('admin.alterarSenha')) class="active" @endif>
                ALTERAR SENHA
            </a>
        </div>
    </nav>
    @endif

    @yield('content')

    <footer>
        <div class="center">
            <div class="informacoes">
                <img src="{{ asset('assets/img/layout/marca-iat.png') }}" alt="" class="iat">
                <div class="informacoes-texto">
                    <p class="telefone">{{ $contato->telefone }}</p>
                    <p>
                        Instituto Alphageos de Tecnologia<br>
                        {!! $contato->endereco !!}
                    </p>
                </div>
            </div>
            <div class="copyright">
                <p>
                    © {{ date('Y') }} {{ config('app.name') }}<br>
                    Todos os direitos reservados.
                </p>
                <p>
                    <a href="http://www.trupe.net" target="_blank">Criação de sites</a>:<br>
                    <a href="http://www.trupe.net" target="_blank">Trupe Agência Criativa</a>
                </p>
            </div>
        </div>
    </footer>

    <script src="//ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
	<script>window.jQuery || document.write('<script src="{{ asset('assets/vendor/jquery/dist/jquery.min.js') }}"><\/script>')</script>
    <script src="{{ asset('assets/js/vendor.main.js') }}"></script>
    <script src="{{ asset('assets/js/main.js') }}"></script>

@if($config->analytics)
    <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
        (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
        ga('create', '', 'auto');
        ga('send', 'pageview');
    </script>
@endif
</body>
</html>
