@extends('admin.common.template')

@section('content')

    <main class="reclamacao-apelacao">
        <div class="center">
            <h2 class="title">RECLAMAÇÃO E APELAÇÃO</h2>

            @include('admin.common._flash')

            <form action="{{ route('admin.reclamacaoApelacao.update', $documentoReclamacaoApelacao->id) }}" method="POST" enctype="multipart/form-data" class="form-documento-admin">
                {!! csrf_field() !!}
                {!! method_field('PATCH') !!}

                <div class="row">
                    <span><strong>ALTERAR DOCUMENTO:</strong></span>
                    <label class="file-input">
                        <span>SELECIONAR DOCUMENTO</span>
                        <input type="file" name="arquivo">
                    </label>
                </div>
                <div class="row link-documento-editar">
                    <span></span>
                    <a href="{{ asset('assets/sistema/reclamacao-apelacao/'.$documentoReclamacaoApelacao->arquivo) }}" target="_blank">{{ $documentoReclamacaoApelacao->arquivo }}</a>
                </div>
                <div class="row">
                    <span>descrição/título:</span>
                    <input type="text" name="titulo" value="{{ $documentoReclamacaoApelacao->titulo }}" required>
                    <input type="submit" value="SALVAR" class="btn btn-primary">
                </div>

                <a href="{{ route('admin.reclamacaoApelacao.index') }}" class="btn btn-default btn-voltar">VOLTAR</a>
            </form>
        </div>
    </main>

@endsection
